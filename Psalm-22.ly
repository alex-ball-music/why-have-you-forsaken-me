\version "2.20.0"
\language "english"
\include "ajb-common.ly"

\header {
  title = "Why have you forsaken me?"
  subtitle = "Based on Psalm 22"
  composer = "Alex Ball"
  % Remove default LilyPond tagline
  copyright = \markup { %\override #'(baseline-skip . 2.5)
  \center-column {
    "© Alex Ball 2020. This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International Licence:"
    "https://creativecommons.org/licenses/by-nc/4.0/"
  } }
  % Remove default LilyPond tagline
  tagline = "v1 – 2020-04-04"
}

global = {
  \key f \minor
  \time 4/4
  \tempo 4=100
}

tenorVoice = \relative c {
  \global
  \dynamicUp
  % Intro
  R1*4 \bar "||"

  % Chorus
  f4\mf r4 r8 f g af
  bf8 af g f f4 e
  f4 r4 r8 c c c
  af'8 af g f ~ f r8 af4
  f4 r4 r8 f g af
  bf8 af g f f4 e
  f4 r4 r8 c c c
  af'8 af g f ~ f4 r \bar"||"

  % Verse 1
  r8 f f f e e c af' ~
  af af bf af af4 g
  r8 f f f e e c c
  af'8 af g f ~ f4 r
  f8 f f f e e c af' ~
  af8 af bf af af4 g
  r8 f f f e e c c
  af'8 af g f r af g f ~ \bar"||"

  % Chorus
  f4 r4 r8 f g af
  bf8 af g f f4 e
  f4 r4 r8 c c c
  af'8 af g f ~ f r8 af4
  f4 r4 r8 f g af
  bf8 af g f f4 e
  f4 r4 r8 c c c
  af'8 af g f ~ f4 r \bar "||"

  % Verse 2
  r8 f f f e e c af' ~
  af af bf af af4 g
  r8 f f f e e c c
  af'8 af g f ~ f4 r
  r8 f f f e e c af' ~
  af8 af bf af af4 g
  r8 f f f e e c c
  af'8 g4 f8 r af g f ~ \bar"||"

  % Chorus
  f4 r4 r8 f g af
  bf8 af g f f4 e
  f4 r4 r8 c c c
  af'8 af g f ~ f r8 af4
  f4 r4 r8 f g af
  bf8 af g f f4 e
  f4 r4 r8 c c c
  af'8 af g f ~ f4 r \bar "||"

  % Middle eight
  R1*4 \bar"||"
  bf8\f bf af g af4 af8 bf
  c8 df bf c8 ~ c4 r
  af4 g8 f g g f g
  af2 r2
  r8 df8 df bf df bf df ef
  df8 df c bf8 ~ bf4 r8 bf
  df c bf df ~ df4 bf
  c2( e4) r4 \bar "||"

  % Verse 3
  r8 f, f f e e c af' ~
  af af bf af af4 g
  r8 f f f e e c c
  af'8 g4 f8 ~ f4 r
  r8 f f f e e c af' ~
  af8 af bf af af4\tenuto g\tenuto
  f8\accent f f f e e c c
  af'8 af g f8 r af g f ~ \bar"||"

  % Chorus
  f4 r4 r8 f\mf g af
  bf8 af g f f4 e
  f4 r4 r8 c c c
  af'8 af g f ~ f r8 af4
  f4 r4 r8 f g af
  bf8 af g f f4 e
  f4 r4 r8 c c c
  af'8\> af g f ~ f4 r\mp

  % Coda
  \override TextSpanner.bound-details.left.text = "rall."
  f4\startTextSpan r4 r8 c c c
  af'2 g4 af\stopTextSpan
  c1\fermata \bar "|."

}

chorus = \lyricmode {
  why __ have you for -- sa -- ken me, O Fa -- ther?
  Why are you so far from my help?
  I cry both day and night but get no res -- pite.
  Why are you so far from my cry?
}

verse = \lyricmode {
  Why have you for -- sa -- ken me, O Fa -- ther?
  Why are you so far from my help?
  I cry both day and night but get no res -- pite.
  Why are you so far from my cry?

  You sit en -- throned on the prai -- ses of our peo -- ple.
  When they cried out to you you lent them your aid.
  Am I such a worm that you can -- not hear me cal -- ling?
  Do you ag -- ree with what they spit in my face?

  Tell me \chorus

  My liv -- ing wa -- ter is poured out in the dust; I’m
  so dry my bones stick out, dis -- joint -- ed and cracked.
  I tried to light up the world, but like a can -- dle,
  my heart with -- in me melts a -- way like wax.

  Tell me \chorus

  You have been my God since be -- fore I was born,
  You are the on -- ly hope I have.
  I hang here help -- less with the hounds at my heels,
  De -- li -- ver me if you can.

  You see the li -- ons who line up to de -- vour me.
  You see the ruf -- fi -- ans who ring me round.
  You see the vul -- tures who vie for what they thieved from me.
  You see the bulls be -- siege and bat -- ter me down.

  So then \chorus

  Why are you so far from my cry?
}

right = \relative c {
  \global
  % Intro
  f8 af f bf ~ bf4 c,
  af'8 g f c' ~ c4 c,
  f8 af r bf r af bf r
  c4\staccato af\staccato f2

  % Chorus
  R1*8

  % Verse 1
  <f c'>2 <g c>
  <c ef>4 <bf df> c <c, e>
  f8 af r bf r af bf r
  c4\staccato af\staccato f2
  R1
  R1
  f8 af r bf r4 bf8 r
  b8 c r f, r2

  % Chorus
  R1*8

  % Verse 2
  \clef treble
  f''2 e
  ef2 d4\staccato e\staccato
  f8 c r e r bf r df
  c c r f, ~ f4 r
  <f af>4 <g bf> <af c> <bf d>
  <c e> <bf d> <af c> <g bf>
  f'8 c r bf r af bf r
  c8 r r f, ~ f2

  % Chorus
  R1*8

  % Middle eight
  \clef bass
  f,8 af f bf ~ bf4 c,
  af'8 g f c' ~ c4 c,
  f8 af r bf r af bf r
  \clef treble
  c4\staccato e\staccato f <f af>
  << {
    <ef g>1
    <ef af>1
    <df f>2 <bf ef>
    <c f>1
  } \\ {
    bf4 af8 g af4. bf8
    c8 df bf c8 ~ c2
    af4 g8 f g4 f8 g
    af1
  } >>
  <df f af>4 r8 q r4 q8 r
  <df f bf>4 r8 q r4 q8 r
  <df g bf>8 q r q r4 q
  <f g c>2 <e g c>2

  % Verse 3
  r8 <f af c> r <e g c> r2
  r8 <ef af c> r <f bf c> r2
  r8 <f af c> r <e bf' c> r4 <f bf df>8 r
  r8 <g c e> r <af c f> r2
  r8 <f af c> r <e g c> r2
  r8 <ef af c> r <f bf c>8 <f bf d>4 <g c e>
  <af c f>8 <af c> r <e bf' c> r4 <f bf df>8 r
  r8 <g c e> r <af c f> r2

% Chorus
  R1*8
  R1*3
}

left = \relative c, {
  \global
  % Intro
  f1 ~
  f2 e
  f2 bf
  c4 c, f2

  % Chorus
  f'8 c af f ~ f2
  g8 af bf g r c af' g
  f8 c af f ~ f4 af
  c8 d e f ~ f4 c
  f8 c af f ~ f2
  g8 af bf g r c af' g
  f8 c af f ~ f4 af
  c8 d e f, ~ f2 ~

  % Verse 1
  f2 g
  af4 bf c c,
  f2 g4 f
  e2 f4 af
  f2 g
  af4 bf c c,
  f2 g4 f
  e2 f2

  % Chorus
  f'8 c af f ~ f2
  g8 af bf g r c af' g
  f8 c af f ~ f4 af
  c8 d e f ~ f4 c
  f8 c af f ~ f2
  g8 af bf g r c af' g
  f8 c af f ~ f4 af
  c8 d e f, ~ f2 ~

  % Verse 2
  f2 g
  af4 bf c c,
  f2 g4 f
  e2 f4 af
  f2 g
  af4 bf c c,
  f2 g4 f
  e2 f2

  % Chorus
  f'8 c af f ~ f2
  g8 af bf g r c af' g
  f8 c af f ~ f4 af
  c8 d e f ~ f4 c
  f8 c af f ~ f2
  g8 af bf g r c af' g
  f8 c af f ~ f4 af
  c8 d e f, ~ f2 ~

  % Middle eight
  f2 bf
  f2 e
  f2 bf
  c4 c, f2
  ef1
  af1
  df,2 ef
  f4 c f ef4
  df2 df'4 c
  bf1
  g1
  c2. c,4

  % Verse 3
  f4. r8 g4. r8
  af4. r8 bf4. r8
  f4. r8 e4 df
  c2 f
  f4. r8 g4. r8
  af4. r8 bf4. r8
  f4. r8 e4 df
  c2 f

  % Chorus
  f'8 c af f ~ f4 r4
  f2. r4
  f'4. f,8 ~ f4 r4
  f4. ~ f8\staccato r2
  f4. ~ f8\staccato r2
  c4. ~ c8\staccato r2
  f4. ~ f8\staccato r2
  R1
  R1*3
}

snippetOne = \relative c {
  f'8 c af bf ~ bf af g f
  g8 af bf df c bf af g
  f'8 c af bf ~ bf af g f
  c8 d e f ~ f2
}

tenorVoicePart = \new Staff \with {
  instrumentName = "T."
  midiInstrument = "choir aahs"
} { \clef "treble_8" \tenorVoice }
\addlyrics { \verse }

pianoPart = \new PianoStaff \with {
  instrumentName = "Org."
} <<
  \new Staff = "right" \with {
    midiInstrument = "church organ"
  } { \clef bass \right }
  \new Dynamics { s1\mp  }
  \new Staff = "left" \with {
    midiInstrument = "church organ"
  } { \clef bass \left }
>>

\score {
  <<
    \tenorVoicePart
    \pianoPart
  >>
  \layout { }
  \midi { }
}

\book {
  \bookOutputSuffix "organ"
  \score {
    <<
      \pianoPart
    >>
    \midi { }
  }
}
