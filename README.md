# Why have you forsaken me?

This piece is based on the first half of Psalm 22, as seen through the lens of
the events of Good Friday. Indeed, I came up with the basics of it on the way
back home from a service of the Liturgy of the Last Hour.

The accompaniment is deliberately sparse to reflect the desolation conveyed by
the psalm. I very nearly made it a single line with no chords or polyphony at
all, but I think this works better. I wanted to give the impression of it
snaking around the singer and jabbing, poking, biting, before deserting them
altogether.

## Summary information

  - *Voicing:* Solo voice (baritone)

  - *Notes on ambitus:* C below middle C to E above.

  - *Instrumentation:* Organ

  - *Approximate performance length:* 3:00

  - *Licence:* Creative Commons Attribution-NonCommercial 4.0 International
     Public Licence: <https://creativecommons.org/licenses/by-nc/4.0/>

## Files

This repository contains the [Lilypond](http://lilypond.org) source code. To
compile it yourself, you will also need my [house style
files](https://gitlab.com/alex-ball-music/ajb-lilypond).

For PDF and MIDI downloads, see the [Releases
page](https://gitlab.com/alex-ball-music/why-have-you-forsaken-me/-/releases).
